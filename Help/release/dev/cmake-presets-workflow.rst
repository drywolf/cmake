cmake-presets-workflow
----------------------

* The :manual:`cmake-presets(7)` format now supports a ``workflowPresets`` field.
